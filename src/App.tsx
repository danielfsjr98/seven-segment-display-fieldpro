import Home from './views/Home'
import {
  ChakraProvider,
  theme,
} from '@chakra-ui/react'

export const App = () => (
  <ChakraProvider theme={theme}>
    <Home />
  </ChakraProvider>
)
