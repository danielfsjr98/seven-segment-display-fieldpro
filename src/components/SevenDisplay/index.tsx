import React, { useMemo } from 'react';
import * as I from './interfaces'
import { Box, Button, Flex, useColorMode } from '@chakra-ui/react';

const SevenDisplay: React.FC<I.SevenDisplayProps> = ({
  activeRow,
  increase
}) => {

  const items = useMemo((): I.Items[] => [
    {
      label: 'a',
      top: '0px',
      left: '20px',
      width: '180px',
      height: '20px'
    },
    {
      label: 'b',
      top: '0px',
      left: '200px',
      width: '20px',
      height: '200px'
    },
    {
      label: 'c',
      top: '200px',
      left: '200px',
      width: '20px',
      height: '200px'
    },
    {
      label: 'd',
      top: '380px',
      left: '20px',
      width: '180px',
      height: '20px'
    },
    {
      label: 'e',
      top: '200px',
      left: '0px',
      width: '20px',
      height: '200px'
    },
    {
      label: 'f',
      top: '0px',
      left: '0px',
      width: '20px',
      height: '200px'
    },
    {
      label: 'g',
      top: '190px',
      left: '20px',
      width: '180px',
      height: '20px'
    },
  ], [])

  const {
    colorMode
  } = useColorMode()

  const getBgColor = (key: I.Key) =>
    activeRow[key] ? 'red.400' : (
      colorMode === 'dark' ? 'gray.600' : 'gray.100' 
    )

  return (
    <Flex
      alignItems='center'
      flexDirection='column'
      rowGap='8'
      p={6}
      backgroundColor={colorMode === 'dark' ? 'gray.700' : 'gray.50'}
      borderRadius='16px'
    >
      <Box
        position='relative'
        w='220px'
        height='400px'
      >
        {items.map(el => 
          <Flex
            justifyContent='center'
            alignItems='center'
            position='absolute'
            key={el.label}
            w={el.width}
            h={el.height}
            top={el.top}
            left={el.left}
            backgroundColor={getBgColor(el.label)}
            borderWidth='1px'
            borderColor={colorMode === 'dark' ? 
              'gray.800' : 'gray.100'
            }
          >
            <Flex 
              borderRadius='50%'
              borderColor='gray.200'
              borderWidth='1px'
              color='gray.800' 
              h='16px'
              w='16px'
              fontSize='12px'
              bgColor='gray.300'
              justifyContent='center'
              alignItems='center'
            >
              {el.label}
            </Flex>
          </Flex>
        )}
      </Box>
      <Button onClick={increase}>
        Increase
      </Button>
    </Flex>
  )
}

export default SevenDisplay;