export interface SevenDisplayProps {
  activeRow: Row
  increase: () => void
}

export interface Row {
  decimal: number
  a: number
  b: number
  c: number
  d: number
  e: number
  f: number
  g:number
}

export interface Items {
  label: Key
  top: string
  left: string
  width: string
  height: string
}

export type Key = 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g'