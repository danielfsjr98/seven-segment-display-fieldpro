import React, { useEffect, useMemo, useState } from 'react';
import { ColorModeSwitcher } from '../../components/ColorModeSwitcher'
import SevenDisplay from '../../components/SevenDisplay';
import {
  Box,
  Grid,
  Flex,
  Heading,
  Table,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
} from '@chakra-ui/react';

const Home: React.FC = () => {
  const [count, setCount] = useState(0);

  const header = useMemo(() =>[
    '',
    'DECIMAL',
    'A',
    'B',
    'C',
    'D',
    'E',
    'F',
    'G'
  ], [])
  
  const rows = useMemo(() => [
    {
      decimal: 0,
      a: 1,
      b: 1,
      c: 1,
      d: 1,
      e: 1,
      f: 1,
      g: 0
    },
    {
      decimal: 1,
      a: 0,
      b: 1,
      c: 1,
      d: 0,
      e: 0,
      f: 0,
      g: 0
    },
    {
      decimal: 2,
      a: 1,
      b: 1,
      c: 0,
      d: 1,
      e: 1,
      f: 0,
      g: 1
    },
    {
      decimal: 3,
      a: 1,
      b: 1,
      c: 1,
      d: 1,
      e: 0,
      f: 0,
      g: 1
    },
    {
      decimal: 4,
      a: 0,
      b: 1,
      c: 1,
      d: 0,
      e: 0,
      f: 1,
      g: 1
    },
    {
      decimal: 5,
      a: 1,
      b: 0,
      c: 1,
      d: 1,
      e: 0,
      f: 1,
      g: 1
    },
    {
      decimal: 6,
      a: 1,
      b: 0,
      c: 1,
      d: 1,
      e: 1,
      f: 1,
      g: 1
    },
    {
      decimal: 7,
      a: 1,
      b: 1,
      c: 1,
      d: 0,
      e: 0,
      f: 0,
      g: 0
    },
    {
      decimal: 8,
      a: 1,
      b: 1,
      c: 1,
      d: 1,
      e: 1,
      f: 1,
      g: 1
    },
    {
      decimal: 9,
      a: 1,
      b: 1,
      c: 1,
      d: 1,
      e: 0,
      f: 1,
      g: 1
    }
  ], [])

  useEffect(() => {
    document.title = `Count: ${count}`
  }, [count])

  const increaseCount = () => {
    setCount(curr =>  {
      if(curr === rows.length - 1) return 0
      return curr + 1
    })
  }

  return (
    <Box fontSize='xl' p={6}>

      <Flex justifyContent='space-between' mb={8}>
        <Heading>Seven-Segment Display</Heading>
        <ColorModeSwitcher />
      </Flex>

      <Grid templateColumns={{lg: '1fr 1fr', md: '1fr'}} gridGap={4}> 
        <SevenDisplay activeRow={rows[count]} increase={increaseCount} />

        <Table variant='simple'>
          <Thead>
            <Tr>
              {header.map((el, index) => 
                <Th key={index} textAlign='start' py={3}> 
                  {el}
                </Th>
              )}
            </Tr>
          </Thead>

          <Tbody>
            {rows.map((el, index) =>
              <Tr 
                key={index} 
                backgroundColor={index === count ? 'gray.100' : ''} 
                color='gray.600' 
                fontSize='xs' 
                fontWeight='bold'
              >
                {/* On/Off status */}
                <Td py={3} maxW='20px'>
                  {count === index && 
                    <Box 
                      w='16px'
                      h='16px'
                      background='red.300'
                      borderRadius='50%'
                      m='auto'
                    />
                  }
                </Td>

                {/* Decimal value */}
                <Td
                  fontSize='16px' 
                  fontWeight='bold'
                  py={3}
                  lineHeight={6}
                >
                  {el.decimal}
                </Td>

                {/* Binary values */}
                {Object.values(el).slice(1)
                  .map((value, i)=> 
                    <Td key={i*Math.random()} py={3}>
                      {value}
                    </Td>
                )}
              </Tr>
            )}
          </Tbody>
        </Table>
      </Grid>

    </Box>
  )
}

export default Home;